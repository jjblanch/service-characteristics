# Implementation

This directory contains the actual samples of GAIA-X Self-Descriptions (SDs). If you want to learn how to create Self-Descriptions instances, please refer to the [Self-Description Tutorial](/documentation/sd-tutorial).

