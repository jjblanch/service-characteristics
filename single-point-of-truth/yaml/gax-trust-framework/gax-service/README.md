# The _gax_service_ Folder
This folder defines the top-level class _ServiceOffering_ and contains subclasses of _ServiceOffering_ to describe artefacts in the Trust Framework with service character.