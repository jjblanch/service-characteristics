# The _gax-configuration_ Folder
This folder describes _Configuration_ artefacts as defined in the Gaia-X Conceptual Model, described in the _gax-core_ folder hierarchy. 
_Configuration_ refers to a set of parameters or attributes used during realization or instantiation of services and resources. 